package com.best.spring.test;


import com.best.spring.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/27
 **/
@ExtendWith(SpringExtension.class)
@ContextConfiguration("classpath:bean1.xml")
//@SpringJUnitConfig(locations = "classpath:bean1.xml")
public class Jtest5 {
    @Autowired
    private User user;

    @Test
    public void test1() {
        user.test();
    }
}
