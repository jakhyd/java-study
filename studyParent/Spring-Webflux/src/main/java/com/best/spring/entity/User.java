package com.best.spring.entity;

import org.springframework.stereotype.Component;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/27
 **/
@Component
public class User {
     public void test() {
         System.out.println("test junit");
     }
}
