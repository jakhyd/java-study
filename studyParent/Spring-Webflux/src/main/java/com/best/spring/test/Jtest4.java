package com.best.spring.test;

import com.best.spring.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/27
 **/
@RunWith(SpringJUnit4ClassRunner.class) //单元测试 框架
@ContextConfiguration("classpath:bean1.xml")
public class Jtest4 {

    @Autowired
    private User user;

    @Test
    public void test1() {
        user.test();
    }

}
