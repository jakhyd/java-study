package com.best.spring.test;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Target;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/27
 **/
@Inherited
@Documented
@Target({ElementType.TYPE, ElementType.FIELD})
public @interface SpringJUnitConfig {

    String locations();
}
