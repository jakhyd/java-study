package com.best.spring.test;

import com.best.spring.entity.User;
import org.springframework.context.support.GenericApplicationContext;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/27
 **/
public class Test {

    @org.junit.Test
    public void testGenericApplicationContext() {
        //1 创建GenericApplicationContext对象
        GenericApplicationContext context = new GenericApplicationContext();
        //2 调用context的方法对象注册
        context.refresh();
        context.registerBean("user1", User.class, ()->new User());
        //3 获取在spring注册的对象
//        User user = (User) context.getBean("com.best.spring.entity.User");
        User user = (User) context.getBean("user1");
        System.out.println(user);
    }
}
