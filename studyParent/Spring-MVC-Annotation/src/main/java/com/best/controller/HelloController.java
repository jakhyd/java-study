package com.best.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/4
 **/
@Controller
@RequestMapping("/hello-controller")
public class HelloController {

    //真实访问地址: 项目名/hello-controller/hello
    @RequestMapping("/hello")
    public String sayHello(Model model) {
        //向模型中添加属性msg与值，可以在jsp页面中取出并渲染
        model.addAttribute("msg", "hello, SpringMVC");
        //jsp名字，/WEB-INF/jsp/hello.jsp
        return "hello";
    }
}
