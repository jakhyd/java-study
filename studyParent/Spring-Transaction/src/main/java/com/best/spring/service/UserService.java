package com.best.spring.service;

import com.best.spring.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/27
 **/
@Service
@Transactional(noRollbackFor = NullPointerException.class, readOnly = false, timeout = -1,
        propagation = Propagation.REQUIRED, rollbackFor = Exception.class, isolation = Isolation.REPEATABLE_READ)
public class UserService {

    //注入dao
    @Autowired
    private UserDao userDao;

    //转账的方法
    public void accountMoney() {
        try {
            //第一步 开启事务
            //第二步 进行业务操作
            //lucy少100
            userDao.reduceMoney();
            //模拟异常
            int i = 10/0;
            //mary多100
            userDao.addMoney();
            //第三步 没有发送异常，提交事务
        } catch (Exception e) {
            // 手动硬编码开启spring事务管理
            //TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            //第四步 出现异常，事务回滚
            //e.printStackTrace();
            throw new RuntimeException();
        }

    }
}
