package com.best.spring.dao;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/27
 **/
public interface UserDao {

    //多钱
    void addMoney();

    //少钱
    void reduceMoney();
}
