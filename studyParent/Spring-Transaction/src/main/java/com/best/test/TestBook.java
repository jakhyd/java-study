package com.best.test;

import com.best.spring.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/27
 **/
public class TestBook {

    @Test
    public void testAccount() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");
        UserService userService = context.getBean("userService", UserService.class);
        userService.accountMoney();
    }

    @Test
    public void testAccount1() {
        ApplicationContext context = new AnnotationConfigApplicationContext("TxConfig.class");
        UserService userService = context.getBean("userService", UserService.class);
        userService.accountMoney();
    }
}
