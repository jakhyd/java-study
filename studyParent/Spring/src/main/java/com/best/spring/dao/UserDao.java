package com.best.spring.dao;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/23
 **/
public interface UserDao {

    void update();
}
