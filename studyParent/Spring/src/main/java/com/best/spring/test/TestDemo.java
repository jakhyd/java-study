package com.best.spring.test;

import com.best.spring.bean.Course;
import com.best.spring.bean.Orders;
import com.best.spring.config.SpringConfig;
import com.best.spring.factorybean.MyBean;
import com.best.spring.module.Book;
import com.best.spring.module.User;
import com.best.spring.service.StudentService;
import com.best.spring.service.UserService1;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/20
 **/
public class TestDemo {

    @Test
    public void testAdd() {
        //1 加载spring配置文件
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");

        //2 获取配置创建的对象
        User user = context.getBean("user", User.class);

        System.out.println(user);

        user.add();
    }
    @Test
    public void testAdd1() {
        User user = new User();
        user.add();
    }

    @Test
    public void testBook1() {
        //1 加载spring配置文件
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean1.xml");

        //2 获取配置创建的对象
        Book book = context.getBean("book", Book.class);
        Book book1 = context.getBean("book", Book.class);
        Book book2 = context.getBean("book", Book.class);

        System.out.println(book1);
        System.out.println(book2);

        book.testDemo();
    }

    @Test
    public void testService() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean7.xml");
        UserService1 userService1 = context.getBean("userService1", UserService1.class);
        System.out.println(userService1);
    }

    @Test
    public void testService2() {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        StudentService studentService = context.getBean("studentService", StudentService.class);
        System.out.println(studentService);
        studentService.add();
    }

    @Test
    public void test3() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean8.xml");
        //实际返回的是Course
        Course course = context.getBean("myBean", Course.class);
        System.out.println(course);
    }

    @Test
    public void testBean() {
//        ApplicationContext context = new ClassPathXmlApplicationContext("bean9.xml");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean9.xml");
        Orders orders = context.getBean("orders", Orders.class);
        System.out.println("第四步 获取创建bean实例对象");
        System.out.println(orders);
        context.close();
    }
}
