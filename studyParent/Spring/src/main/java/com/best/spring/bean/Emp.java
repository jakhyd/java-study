package com.best.spring.bean;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/23
 **/
//员工类
public class Emp {

    private String ename;
    private String gender;

    //生成dept的get方法
    public Dept getDept() {
        return dept;
    }

    //员工属于某一个部门，使用对象形式表示
    private Dept dept;

    public void setEname(String ename) {
        this.ename = ename;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }
}
