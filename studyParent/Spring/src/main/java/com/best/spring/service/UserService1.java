package com.best.spring.service;

import org.springframework.stereotype.Component;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/23
 **/
/**
 * 在注解里面value属性值可以省略不写
 * 默认值是类名称，首字母小写
 * UserService --userService
 */

@Component(value = "userService1") //创建对象，等价于<bean id="userService1" class=""/>
public class UserService1 {

    public void add() {
        System.out.println("service add....");
    }
}
