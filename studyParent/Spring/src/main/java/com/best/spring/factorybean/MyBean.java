package com.best.spring.factorybean;

import com.best.spring.bean.Course;
import org.springframework.beans.factory.FactoryBean;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/23
 **/
public class MyBean implements FactoryBean<Course> {

    //定义返回bean
    @Override
    public Course getObject() throws Exception {
        Course course = new Course();
        course.setCname("abc");
        return course;
    }

    @Override
    public Class<?> getObjectType() {
        return null;
    }
}
