package com.best.spring.bean;

import java.util.List;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/23
 **/
public class Books {
    private List<String> list;

    public void setList(List<String> list) {
        this.list = list;
    }
}
