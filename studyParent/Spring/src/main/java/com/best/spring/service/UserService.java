package com.best.spring.service;

import com.best.spring.dao.UserDao;
import com.best.spring.dao.UserDaoImpl;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/23
 **/
public class UserService {

    //创建UserDao类型属性，生成set方法
    private UserDao userDao;
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void add() {
        System.out.println("service add .....");

        //原始方式: 创建UserDao对象
        UserDao userDao = new UserDaoImpl();
        userDao.update();
    }
}
