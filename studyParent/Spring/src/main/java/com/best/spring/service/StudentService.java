package com.best.spring.service;

import com.best.spring.dao.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/23
 **/
@Service
public class StudentService {

    @Value(value = "abc")
    private String name;

    //定义dao类型属性
    //不需要添加set方法
    //添加注入属性注解
//    @Autowired  //根据名称进行注入
//    @Qualifier(value = "studentDaoImpl1")

    //@Resource //根据类型进行注入
    @Resource(name = "studentDaoImpl1") //根据名称进行注入
    private StudentDao studentDao;

    public void add() {
        System.out.println("service add ....");
        studentDao.add();
    }
}
