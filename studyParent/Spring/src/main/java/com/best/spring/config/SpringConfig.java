package com.best.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/23
 **/
@Configuration //作为配置类，替代xml配置文件
@ComponentScan(basePackages = {"com.best"})
public class SpringConfig {
}
