package com.best.spring.dao;

import org.springframework.stereotype.Repository;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/23
 **/
@Repository(value = "studentDaoImpl1")
public class StudentDaoImpl implements StudentDao {
    @Override
    public void add() {
        System.out.println("dao add .....");
    }
}
