package com.best.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/5
 **/
@Controller
public class EncodingController {

    @RequestMapping("/e/t")
    public String test1(String name, Model model) {
        System.out.println(name);
        //获取表达提交的值
        model.addAttribute("msg", name);
        //跳转到test页面显示输入的值
        return "test";
    }
}
