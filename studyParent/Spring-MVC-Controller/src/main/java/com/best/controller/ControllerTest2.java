package com.best.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/4
 **/
//@Controller注解的类会自动添加到Spring上下文中
//被这个注解的类，中的所有方法，如果返回值是String，并且有具体页面可以跳转，那么就会被视图解析器解析
@Controller
public class ControllerTest2 {

    //映射访问路径
    @RequestMapping("/t3")
    public String test3(Model model) {
        //Spring MVC会自动实例化一个Model对象用于向视图中传值
        model.addAttribute("msg", "ControllerTest2");
        //返回视图位置
        return "test";
    }

    @RequestMapping("/t4")
    public String test4(Model model) {
        model.addAttribute("msg", "test3");
        return "test";
    }
}
