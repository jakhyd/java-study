package com.best.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/5
 **/
@Controller
public class RestFulController {

    //映射访问路径
    @RequestMapping(value = "/commit/{p1}/{p2}")
    public String index(@PathVariable int p1, @PathVariable String p2, Model model) {
        String result = p1 + p2;
        //Spring MVC会自动实例化一个Model对象用于向视图中传值
        model.addAttribute("msg", "结果： " + result);
        //返回视图位置
        return "test";
    }

    //映射访问路径，必须是POST请求
    @RequestMapping(value = "/hello", method = {RequestMethod.POST})
    public String index(Model model) {
        model.addAttribute("msg", "hello");
        return "test";
    }
}
