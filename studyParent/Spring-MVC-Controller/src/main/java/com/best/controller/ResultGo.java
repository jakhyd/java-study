package com.best.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/5
 **/
@Controller
public class ResultGo {

    //输出
    @RequestMapping("/result/t1")
    public void test1(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.getWriter().println("hello");
    }

    //重定向
    @RequestMapping("/result/t2")
    public void test2(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect("/index.jsp");
    }

    //转发
    @RequestMapping("/result/t3")
    public void test3(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        req.setAttribute("msg", "/result/t3");
        req.getRequestDispatcher("/WEB-INF/jsp/test.jsp").forward(req, resp);
    }
}
