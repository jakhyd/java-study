package com.best.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/4
 **/
@Controller
@RequestMapping("/admin")
public class ControllerTest3 {

    @RequestMapping("/test5")
    public String test3(Model model) {
        model.addAttribute("msg", "ControllerTest3");
        return "test";

    }
}
