package com.best.spring.aopxml;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/26
 **/
public class BookProxy {
    public void before() {
        System.out.println("before....");
    }
}
