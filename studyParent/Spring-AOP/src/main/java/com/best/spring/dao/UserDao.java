package com.best.spring.dao;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/26
 **/
public interface UserDao {
    int add(int a, int b);
    String update(String id);
}
