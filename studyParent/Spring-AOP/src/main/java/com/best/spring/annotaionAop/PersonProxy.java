package com.best.spring.annotaionAop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/26
 **/
@Component
@Aspect
@Order(2)
public class PersonProxy {

    //前置通知
    //@Before注解表示作为前置通知(..)表示方法参数列表
    @Before(value = "execution(* com.best.spring.annotaionAop.User.add(..))")
    public void before() {
        System.out.println("person before.....");
    }
}
