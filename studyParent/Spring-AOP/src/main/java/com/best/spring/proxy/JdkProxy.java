package com.best.spring.proxy;

import com.best.spring.dao.UserDao;
import com.best.spring.dao.UserDaoImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/26
 **/
public class JdkProxy {
    public static void main(String[] args) {
        //创建接口实现类代理对象
        Class[] interfaces = {UserDao.class}; //代理目标类实现的接口
        //方式一: 匿名内部类的方式
        /**
        Proxy.newProxyInstance(JdkProxy.class.getClassLoader(), interfaces, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                return null;
            }
        });*/
        //方式二: 实现InvocationHandler接口
        UserDaoImpl userDao = new UserDaoImpl();
        UserDao dao = (UserDao) Proxy.newProxyInstance(JdkProxy.class.getClassLoader(), interfaces, new UserDaoProxy(userDao));
        int result = dao.add(1, 2);
        System.out.println("result:" + result);
    }
}


//创建代理对象代码
class UserDaoProxy implements InvocationHandler {

    //1 把代理的目标对象传过来
    //有参数构造传递
    private Object obj;
    public UserDaoProxy(Object obj) {
        this.obj = obj;
    }

    //增强的逻辑
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //方法之前
        System.out.println("方法之前执行..." + method.getName() + ": 传递的参数..." + Arrays.toString(args));
        //被增强的方法执行
        Object invoke = method.invoke(obj, args);
        //方法之后
        System.out.println("方法之后执行...." + obj);
        return invoke;
    }
}
