package com.best.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/26
 **/
@Configuration
@ComponentScan(basePackages = {"com.best"})
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class ConfigAop {
}
