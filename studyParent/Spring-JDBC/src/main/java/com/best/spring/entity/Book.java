package com.best.spring.entity;

import lombok.*;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/27
 **/
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    private String userId;
    private String username;
    private String ustatus;
}
