package com.best.spring.dao;

import com.best.spring.entity.Book;

import java.util.List;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/27
 **/
public interface BookDao {

    //添加的方法
    void add(Book book);

    //修改的方法
    void updateBook(Book book);

    //删除的方法
    void delete(String id);

    //查询表记录数
    int selectCount();

    //查询返回对象
    Book findBookInfo(String id);

    //查询返回集合
    List<Book> findAllBook();

    void batchAddBook(List<Object[]> batchArgs);

    void batchUpdateBook(List<Object[]> batchArgs);

    void batchDeleteBook(List<Object[]> batchArgs);
}
