package com.best.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2021/6/12
 **/
// 实现java config第一步： 写个类实现WebApplicationInitializer
    // @WebServlet
public class MyWebApplicationInitializer implements WebApplicationInitializer {

    // web容器会在启动的时候去调用orlStartup() ServletContext web上下文对象
    // servlet 3.0版本以后提出的一个新规范SPI
    // "你”的项目里面如果有某些类或者某些方法需要在启动的时候被Tomcat(Web容器)调用的话
    // 首先你在你的项目的根目录的META-INF/services/javax.servlet.ServletContainerInitializer目录下建立一个文件 spring + springmvc

    @Override
    public void onStartup(ServletContext servletContext) {

        // 用java注解的方式，去初始化spring上下文环境
        // 与AnnotationConfigApplicationContext\ClassPathXmlApplicationContext类似
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        // spring程序(容器)上下文对象

        context.register(AppConfig.class);

        // Create and register the DispatcherServlet
        DispatcherServlet servlet = new DispatcherServlet(context);
        ServletRegistration.Dynamic registration = servletContext.addServlet("app", servlet);
        registration.setLoadOnStartup(1);
        registration.addMapping("/app/*");
    }
}