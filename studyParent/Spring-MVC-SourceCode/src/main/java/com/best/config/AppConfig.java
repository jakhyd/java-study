package com.best.config;

import org.eclipse.core.internal.utils.Convert;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2021/6/12
 **/
@Configuration // 声明它是一个配置类
@ComponentScan("com.best")
public class AppConfig extends WebMvcConfigurationSupport {

}
