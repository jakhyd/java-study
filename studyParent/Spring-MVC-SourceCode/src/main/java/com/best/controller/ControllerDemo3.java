package com.best.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2021/6/12
 **/

/**
 * controller 定义的方式，有2种类型和3种实现
 * 2中类型：BeanName类型和@Controller类型
 * 3种实现：1.实现HttpRequestHandler 2.实现Controller 3.@Controller
 */
@Controller
public class ControllerDemo3 {

    /**
     * Spring MVC 找Controller流程:
     *  1.扫描整个项目，Spring已经做了，定义一个Map集合
     *  2.拿到所有加了@Controller注解的类
     *  3.遍历类里面所有的方法对象
     *  4.判断方法是否加了@RequestMapping注解
     *  5.把@RequestMapping注解的value，作为Map集合的key给put进去，把方法对象作为value放入Map集合
     *  6.根据用户发送的请求，拿到请求中的URI url:http://localhost:8080/demo3 uri:/demo3
     *  7.使用请求的uri作为Map的key 去map里面get 看看是否有返回值
     */

    //真实访问地址: 项目名/demo3
    @RequestMapping("/demo3")
    public String sayHello(Model model) {
        //向模型中添加属性msg与值，可以在jsp页面中取出并渲染
        model.addAttribute("msg", "方式三: @Controller注解");
        //jsp名字，/WEB-INF/jsp/hello.jsp
        return "hello";
    }

    @RequestMapping("/demo3/{id}")
    @ResponseBody
    public String test(@PathVariable("id") String id) {
        return id;
    }
}