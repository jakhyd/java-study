package com.best.controller;


import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2021/6/13
 **/
public class ControllerDemo1 implements Controller {
    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("方式一: 实现Controller接口" );
        // 使用ModelAndView去响应
        // 模型数据和视图对象
        ModelAndView modelAndView = new ModelAndView();
        // 添加模型数据
        modelAndView.addObject("msg", "方式一: 实现Controller接口");
        // 设置视图路径，转发
        modelAndView.setViewName("/hello");

        return modelAndView;
    }
}
